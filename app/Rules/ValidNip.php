<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * Check if given number is a valid Polish tax ID (NIP)
 */
class ValidNip implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $strValue = (string) $value;

        if (preg_match('/^[0-9]{10}$/', $strValue) != 1) {
            return false;
        }

        $digits = str_split($strValue);
        $checkSum = 0;
        foreach ([6, 5, 7, 2, 3, 4, 5, 6, 7] as $iter => $multiplier) {
            $checkSum += $multiplier * $digits[$iter];
        }
        $checkDigit = $checkSum % 11;

        return $checkDigit == $digits[9];
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Given NIP number is incorrect.');
    }
}
