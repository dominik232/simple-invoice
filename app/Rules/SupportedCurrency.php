<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class SupportedCurrency implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return in_array($value, config('currency.supported', []));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The currenct is not supported.');
    }
}
