<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceRequest extends FormRequest
{
    /**
     * Modify validator in order to translate field names
     *
     * @return \Illuminate\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $validator->setAttributeNames([
            'number' => __('Invoice number'),
            'nip_buyer' => __('NIP buyer'),
            'nip_seller' => __('NIP seller'),
            'product_name' => __('Product name'),
            'value_netto' => __('Value NETTO'),
            'currency_code' => __('Currency code'),
            'issued_at' => __('Issued at'),
        ]);

        return $validator;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => ['required', 'max:15'],
            'nip_buyer' => [
                'required',
                app()->make(\App\Rules\ValidNip::class),
            ],
            'nip_seller' => [
                'required',
                app()->make(\App\Rules\ValidNip::class),
            ],
            'product_name' => ['required', 'max:32'],
            'value_netto' => ['required', 'numeric'],
            'currency_code' => [
                'required',
                app()->make(\App\Rules\SupportedCurrency::class),
            ],
            'issued_at' => ['required', 'date'],
        ];
    }
}
