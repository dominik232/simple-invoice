<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    /**
     * Get list of supported currencies
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return response()->json([
            'data' => config('currency.supported'),
        ]);
    }
}
