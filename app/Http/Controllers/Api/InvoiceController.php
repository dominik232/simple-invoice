<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateInvoiceRequest;
use App\Http\Requests\UpdateInvoiceRequest;
use App\Models\Invoice;

class InvoiceController extends Controller
{
    const INDEX_ORDER_BY = 'created_at';
    const INDEX_ORDER_DIRECTION = 'desc';

    /**
     * Get list of invoices
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Invoice::orderBy($this::INDEX_ORDER_BY, $this::INDEX_ORDER_DIRECTION)->paginate();
    }

    /**
     * Create a new invoice
     *
     * @param InvoiceRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(CreateInvoiceRequest $request)
    {
        $invoice = Invoice::create($request->all());

        return response()->json(['data' => $invoice]);
    }

    /**
     * Update existing invoice
     *
     * @param InvoiceRequest $request
     * @param Invoice $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInvoiceRequest $request, Invoice $invoice)
    {
        $invoice->update($request->all());

        return response()->json(['data' => $invoice]);
    }

    /**
     * Delete an invoice
     *
     * @param Invoice $invoice
     * @return \Illuminate\Http\Response
     */
    public function delete(Invoice $invoice)
    {
        $invoice->delete();

        return response()->json([]);
    }
}
