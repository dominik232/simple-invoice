<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Akaunting\Money\Currency;
use Akaunting\Money\Money;

class Invoice extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    protected $fillable = [
        'number',
        'nip_buyer',
        'nip_seller',
        'product_name',
        'value_netto',
        'currency_code',
        'issued_at',
    ];

    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'issued_at',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'value_netto_formatted',
    ];

    /**
     * Format money according to the currency
     *
     * @return Money
     */
    public function getValueNettoFormattedAttribute()
    {
        return (new Money((float) $this->value_netto, new Currency($this->currency_code), true))->format();
    }
}
