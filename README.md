# Simple Invoice

## Requirements
* PHP 7.4
* Composer
* NodeJS 14.x
* NPM
* (optional) PHP SQLite extension

## Installation
1. Install backend dependencies

    ```
    composer install
    ```

2. Install frontend dependencies

    ```
    npm install
    ```

3. Build frontend assets

    ```
    npm run production # or dev, watch etc
    ```

4. Migrate database (when using SQLite, it should just work after `composer install` without any farther configuration)

    ```
    php artisan migrate
    ```

5. Run development server

    ```
    php artisan serve
    ```

6. (Optional) Seed the database

    ```
    php artisan db:seed
    ```
