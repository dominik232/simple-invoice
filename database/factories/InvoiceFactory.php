<?php

namespace Database\Factories;

use App\Models\Invoice;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class InvoiceFactory extends Factory
{
    const NIPS_BUYER = [
        '0855911968',
        '5252926455',
        '9304145023',
        '5066051559',
        '3759275477',
        '7112053560',
        '7070686288',
        '0297019323',
        '7175320002',
        '8940318452',
    ];

    const NIPS_SELLER = [
        '5445816428',
        '7099211130',
    ];

    const PRODUCTS = [
        'SKP',
        'FEX',
        'SAM',
    ];

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Invoice::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'number' => 'F/'.$this->faker->unique()->numberBetween(1000, 9000).'/'.now()->format('Y'),
            'nip_buyer' => $this->faker->randomElement($this::NIPS_BUYER),
            'nip_seller' => $this->faker->randomElement($this::NIPS_SELLER),
            'product_name' => $this->faker->randomElement($this::PRODUCTS),
            'value_netto' => $this->faker->randomFloat(2, 65, 1500),
            'currency_code' => $this->faker->randomElement(config('currency.supported', ['PLN'])),
            'issued_at' => $this->faker->dateTimeBetween('-3 months', 'now'),
        ];
    }
}
