import Vue from 'vue';

import Invoices from './Invoices.vue';
import InvoiceForm from './InvoiceForm.vue';
import FormInput from './FormInput.vue';

[
    Invoices,
    InvoiceForm,
    FormInput,
].forEach((component) => {
    Vue.component(component.name, component);
});
