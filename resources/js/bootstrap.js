import Vue from 'vue';
import VueI18n from 'vue-i18n';
import messages from './i18n';

require('bootstrap');

window._ = require('lodash');

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.jQuery = require('jquery');

Vue.use(VueI18n);
const i18n = new VueI18n({
    locale: window.app_locale,
    messages,
});

new Vue({i18n}).$mount('#app');
