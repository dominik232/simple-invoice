export default {
    en: {
        invoice: {
            number: 'Invoice no',
            nip_buyer: 'Buyer NIP',
            nip_seller: 'Seller NIP',
            product: 'Product',
            value_netto: 'Netto value',
            issued_at: 'Issued at',
            updated_at: 'Updated at',
            create: 'Create invoice',
            currency: 'Currency',
        },
        functions: 'Functions',
        edit: 'Edit',
        delete: 'Delete',
        pagination: {
            prev: 'Go to previous page',
            next: 'Go to next page',
        },
        warnings: {
            delete: 'You are going to delete invoice {number}. Do you want to proceed?',
        },
        form: {
            editing: 'Editing invoice {number}',
            creating: 'Create a new invoice',
            save: 'Save',
        },
        request: {
            error: 'There was an error during the request.',
        },
    },

    pl: {
        invoice: {
            number: 'Nr faktury',
            nip_buyer: 'NIP kupującego',
            nip_seller: 'NIP sprzedającego',
            product: 'Produkt',
            value_netto: 'Kwota netto',
            issued_at: 'Data wystawienia',
            updated_at: 'Data edycji',
            create: 'Utwórz fakturę',
            currency: 'Waluta',
        },
        functions: 'Funkcje',
        edit: 'Edytuj',
        delete: 'Usuń',
        pagination: {
            prev: 'Idź do poprzedniej strony',
            next: 'Idź do następnej strony',
        },
        warnings: {
            delete: 'Zamierzasz usunąć fakturę {number}. Czy chcesz kontynuować?',
        },
        form: {
            editing: 'Edycja faktury {number}',
            creating: 'Utwórz nową fakturę',
            save: 'Zapisz',
        },
        request: {
            error: 'Podczas wykonywania zapytania wystąpił błąd.',
        },
    },
};
