@extends('layout')

@section('content')
    <div class="container">
        <h1>{{ __('Invoices') }}</h1>
        <invoices />
    </div>
@endsection
