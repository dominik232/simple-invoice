<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <script>window.app_locale = '{{ config('app.locale') }}'</script>
    </head>

    <body>
        <div id="app">
            @yield('content')
        </div>
    </body>

    <script src="{{ asset('js/app.js') }}"></script>
</html>
