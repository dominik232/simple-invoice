<?php

return [
    'supported' => [
        'PLN',
        'USD',
        'EUR',
        'GBP',
    ],
];
