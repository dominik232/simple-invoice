<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('invoice')->group(function () {
    Route::get('index', 'InvoiceController@index');
    Route::post('create', 'InvoiceController@create');
    Route::put('{invoice}/update', 'InvoiceController@update');
    Route::delete('{invoice}/delete', 'InvoiceController@delete');
});

Route::prefix('currency')->group(function () {
    Route::get('index', 'CurrencyController@index');
});
